import { questionArr as dataRaw } from "../data/questions.js";
import {
  checkFillInput,
  checkSingleChoice,
  renderNumberQuestion,
  renderQuestion,
  showResut,
} from "./questionController.js";
let questionArr = dataRaw.map((question) => {
  return { ...question, isCorect: null };
});
console.log("questionArr: ", questionArr);

let currentQuestionIndex = 0;

// render lần đầu

renderNumberQuestion(currentQuestionIndex, questionArr.length);

renderQuestion(questionArr[currentQuestionIndex]);

// khi user nất next

document.getElementById("nextQuestion").addEventListener("click", function () {
  // kiểm tra câu hỏi hiện tại
  if (questionArr[currentQuestionIndex].questionType == 1) {
    // check loại 1
    questionArr[currentQuestionIndex].isCorect = checkSingleChoice();
  } else {
    // check loai 2
    questionArr[currentQuestionIndex].isCorect = checkFillInput();
  }
  console.log("questionArr: ", questionArr);
  currentQuestionIndex++;
  // khi user làm xong câu hỏi cuối cùng
  if (currentQuestionIndex == questionArr.length) {
    showResut();
    return;
  }

  // render câu hỏi tiếp theo
  renderNumberQuestion(currentQuestionIndex, questionArr.length);

  let question = questionArr[currentQuestionIndex];
  renderQuestion(question);
});
