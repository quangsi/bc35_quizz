export let renderNumberQuestion = (index, total) => {
  document.getElementById("currentStep").innerHTML = `
  <h2> ${index + 1}/ ${total} </h2>`;
};

let renderRadioButton = (data) => {
  return `<div class="form-check w-100">
  <label class="form-check-label">
    <input    type="radio" class="form-check-input" name="singleChoice"
     value=${data.exact}
    >${data.content}
  </label>
</div>`;
};
let renderSigleChoice = (question) => {
  let contentAnswers = "";
  question.answers.forEach((item) => {
    contentAnswers += renderRadioButton(item);
  });
  document.getElementById("contentQuizz").innerHTML = `
    <h5>${question.content}</h5>
    ${contentAnswers}

  `;
};

let renderFillInput = (question) => {
  let contentHTML = `
    <h5>${question.content}</h5>
    <div class="form-group w-100">
  
    <input type="text" class="form-control text-white" placeholder="Câu trả lời"
    
    data-not-answer='${question.answers[0].content}'
    
    id="fillInput">
  </div>
`;
  document.getElementById("contentQuizz").innerHTML = contentHTML;
};

export let renderQuestion = (quetion) => {
  if (quetion.questionType == 1) {
    renderSigleChoice(quetion);
  } else {
    renderFillInput(quetion);
  }
};

export let showResut = () => {
  document.getElementById("quizResult").classList.add("d-block");
  document.getElementById("quizStart").style.display = "none";
};

// check cau tra loi user

export let checkSingleChoice = () => {
  let userAnswer = document.querySelector(
    'input[name="singleChoice"]:checked'
  ).value;
  console.log("userAnswer: ", userAnswer);
  if (userAnswer == "true") {
    return true;
  } else {
    return false;
  }
};

export let checkFillInput = () => {
  let inputEl = document.getElementById("fillInput");
  console.log("inpulEl: ", inputEl.dataset);

  return inputEl.value == inputEl.dataset.notAnswer;
};
